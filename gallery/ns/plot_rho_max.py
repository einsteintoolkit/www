#!/usr/bin/python3
#Script developed by Peter Deiner (diener@cct.lsu.edu)
from kuibit.simdir import SimDir
import matplotlib.pyplot as plt
PATH1="/data/data_maxwell/tovgallery/tovres1"
PATH2="/data/data_maxwell/tovgallery/tovres2"
PATH3="/data/data_maxwell/tovgallery/tovres3"
PATH4="/data/data_maxwell/tovgallery/tovres4a3"

rhol = SimDir(PATH1).timeseries.maximum['rho']
rhom = SimDir(PATH2).timeseries.maximum['rho']
rhoh = SimDir(PATH3).timeseries.maximum['rho']
rhoh2 = SimDir(PATH4).timeseries.maximum['rho']

plt.figure()
plt.plot(rhol / rhol(0))
plt.plot(rhom / rhom(0))
plt.plot(rhoh / rhoh(0))
plt.plot(rhoh2 / rhoh2(0))
plt.legend(['low','med','hi','hi2'], loc='lower right')
plt.xlabel(r'$t[M_{sun}]$')
plt.ylabel(r'$\rho_c/\rho_c(0)$')
plt.title("Maximum Density vs Time")
plt.tick_params(axis='both',direction='in',top=True,right=True)
plt.savefig("density_p.pdf", dpi = 300)
plt.savefig("density_p.png", dpi = 300)

plt.figure(2)
rhomres=rhom.fixed_timestep_resampled(3.2)
rhohres=rhoh.fixed_timestep_resampled(3.2)
rhoh2res= rhoh2.fixed_timestep_resampled(3.2)
plt.plot(rhomres-rhohres)
plt.plot(1.4*(rhohres-rhoh2res))
plt.plot(1.7733*(rhohres-rhoh2res))
plt.legend(['med-hi','1.4*(hi-hi2)','1.7733*(hi-hi2)'],loc='lower right')
plt.xlabel(r'$t[M_{sun}]$')
plt.ylabel(r'$\Delta \rho_c/\rho_c(0)$')
plt.title("Difference in Maximum Density vs Time")
plt.tick_params(axis='both',direction='in',top=True,right=True)
plt.savefig("convergence_p.pdf", dpi = 300)
plt.savefig("convergence_p.png", dpi = 300)
