import matplotlib.pyplot as plt
import numpy as np
import sys
import re

for a in sys.argv[1:]:
    data = np.genfromtxt(a)
    for i in range(len(data[:,0])):
        data[i,0] = i
    print(data[:,0])
    g = re.search(r'(\d\d\d\d)(\d\d)(\d\d)', a)
    if g:
        label = f"{g.group(1)}/{g.group(2)}/{g.group(3)}"
    else:
        label = a
    plt.semilogy(data[:,0], data[:,1],label=label)
plt.title("2nd column terr_norm_eqn0.asc")
plt.xlabel("iteration number")
plt.ylabel("Solution error")
plt.grid()
plt.legend()
plt.savefig("err.png")
