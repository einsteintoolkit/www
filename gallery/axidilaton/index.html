<!DOCTYPE html>

<html lang="en">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <script src="../../head.js" type="text/javascript"></script>
    <title>Black-hole hair in axi-dilaton gravity</title>
</head>

<body>
<header>
    <script src="../../menu.js" type="text/javascript">
    </script>
    <script type="text/javascript" async
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML" async>
    </script>
</header>
<div class="container">
    <div class="row">


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section">

            <h2>Gallery: Black-hole hair in axi-dilaton gravity</h2>
            <p>This simulation evolves a single black hole in a theory of modified gravity called "axi-dilaton" gravity
                (<a href="https://journals.aps.org/prd/abstract/10.1103/PhysRevD.105.044022">Cano &amp; Ruip&eacute;rez 2022</a>,
                <a href="https://journals.aps.org/prd/abstract/10.1103/PhysRevD.52.3506">Kanti &amp; Tamvakis 1995</a>) using the
                <a href="https://bitbucket.org/canuda/canuda_axidilaton">AxiDilaton</a> arrangement as a part of the
                Canuda code for
                exploring fundamental physics. This model of gravity includes a pseudo-scalar (or axion) field,
                \(\Theta\), and a scalar (or dilaton) field, \(\Phi\), which are kinetically coupled to one another.
                In this simulation, we evolve these fields in the background of a single rotating black hole until they
                settle to non-trivial profiles around the black hole
                which we refer to as axion and dilaton hair. The fields are initialized to be the analytical axion and
                dilaton hair
                solutions in the low-spin, low-coupling approximation in axi-dilaton gravity
                (<a href="https://journals.aps.org/prd/abstract/10.1103/PhysRevD.105.044022">Cano &amp; Ruip&eacute;rez 2022</a>2).
                For both the axion and dilaton fields, we
                present the time evolution of the leading order
                multipoles at a given extraction radius as well as two-dimensional profiles sliced perpendicular to the
                equatorial
                plane. The <a href="https://bitbucket.org/canuda/canuda_axidilaton">AxiDilaton</a> arrangement and an
                analysis of the
                axion and dilaton hair growth in a black hole spacetime are presented in <a href="https://arxiv.org/abs/2501.14034">Richards
                    et. al. 2025</a>.
            </p>

            <p>The plots show the leading order scalar multipoles, \(\Theta_{10}\) and \(\Theta_{30}\) as well as
                \(\Phi_{00}\) and \(\Phi_{20}\), evolving in the background of
                a black hole with spin \(\chi = 0.9\) extracted at \(r_{ex} = 20M\) and with a coupling strength of \(\hat{a}_{s}=0.01\). These modes give the leading order
                contributions to
                the axion and dilaton hairs, respectively. Finally, we include a two-dimensional slice along the axis of
                rotation in
                the xz-plane for both the axion and dilaton fields at a late time, once the fields have settled to their
                final profiles.
            </p>

            <h3>Simulation details</h3>

            <h4>Computational details</h4>
            <table style="word-wrap: break-word;">
                <tr>
                    <th>Thornlist</th>
                    <td>
                        <a href="https://bitbucket.org/canuda/canuda_axidilaton/raw/main/axidilaton.th">axidilaton.th</a>
                    </td>
                </tr>
                <tr>
                    <th>Parameter File</th>
                    <td><a href="https://bitbucket.org/canuda/canuda_axidilaton/raw/main/par/single_bh_axidilaton.par">single_bh_axidilaton.par</a>
                    </td>
                </tr>
                <tr>
                    <th>Submission command</th>
                    <td>
                        <code>simfactory/bin/sim create-submit aSpt01_apt9 --config sim --parfile repos/Canuda_AxiDilaton/par/single_bh_axidilaton.par --procs 112 --machine frontera --queue small</code>
                    </td>
                </tr>
                <tr>
                    <th>Total memory</th>
                    <td>63 GB</td>
                </tr>
                <tr>
                    <th>Run time</th>
                    <td>Approximately 7 hours using two nodes (112 cores) on Frontera or ~800 CPU hours</td>
                </tr>
                <tr>
                    <th>Results (gzip 496MB, uncompressed 657MB)</th>
                    <td><a href="https://bitbucket.org/einsteintoolkit/www/downloads/single_bh_axidilaton-20241206.tar.gz">single_bh_axidilaton-20241206.tar.gz</a></td>
                </tr>
            </table>
            <p>This example was last tested on 6-Dec-2024.</p>

        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 section">
            <a class="thumbnail" href="mp_Theta_l1_m0_r20.00.svg"><img alt="Dominant mode of the axion field (Theta)" src="mp_Theta_l1_m0_r20.00.svg"></a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 section">
            <a class="thumbnail" href="mp_Phi_l0_m0_r20.00.svg"><img alt="Dominant mode of the dilaton field (Phi)" src="mp_Phi_l0_m0_r20.00.svg"></a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 section">
            <a class="thumbnail" href="mp_Theta_l3_m0_r20.00.svg"><img alt="Largest subdominant mode of the axion field (Theta)" src="mp_Theta_l3_m0_r20.00.svg"></a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 section">
            <a class="thumbnail" href="mp_Phi_l2_m0_r20.00.svg"><img alt="Largest subdominant mode of the dilaton field (Phi)" src="mp_Phi_l2_m0_r20.00.svg"></a>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section">
            <p>The evolution of dominant and subdominant modes of the axion (\(\Theta\)) and dilaton (\(\Phi\)) fields
                around a Kerr black
                hole of spin \(\chi = 0.9\) extracted at \(r_{ex} = 20M\) and with a coupling strength of
                \(\hat{a}_{s}=0.01\). These plots can be generated using <a
                    href="AxiDilGalleryExample_timeseries.py">AxiDilGalleryExample_timeseries.py</a>. See the bottom
                of the file for an example of how to use the script.
            </p>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section">
            <a class="thumbnail" href="xz_t100.png"><img alt="Snapshot of axion and dilaton fields" src="xz_t100.png"></a>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section">
            <p>Axion (\(\Theta\)) and dilaton (\(\Phi\)) field profiles in a slice perpendicular to the equatorial
                plane. This plot can be generated using <a href="https://sbozzolo.github.io/kuibit">Kuibit</a> with
                <a href="AxiDilGalleryExample_xz_snapshot.py">AxiDilGalleryExample_xz_snapshot.py</a>. See the bottom
                of the file for an example of how to use the script.
            </p>
        </div>

        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
            <script src="../../footer/footer.js" type="text/javascript"></script>
        </div>
    </div>
</div>

</body>
</html>
