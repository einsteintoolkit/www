function NSFLink(id) {
  return "<a href='https://nsf.gov/awardsearch/showAward?AWD_ID="+id+"&HistoricalAwards=false'>"+
         id+"</a>";
}

document.write(
  '        <div class="footer">'+
  '        <hr class="separator" />'+
  '        The Einstein Toolkit has been supported by NSF '+
          NSFLink('2004157')+'/'+
          NSFLink('2004044')+'/'+
          NSFLink('2004311')+'/'+
          NSFLink('2227105')+'/'+
          NSFLink('2004879')+'/'+
          NSFLink('2003893')+'/'+
          NSFLink('2114582')+'.'+
  '        Any opinions, findings, and conclusions or recommendations expressed in this material '+
  '        are those of the author(s) and do not necessarily reflect the views of the National Science Foundation.'+
  '        </div>'
  );


      
        
        
         
        
        
