% vim: tw=0
# Release Announcement

We are pleased to announce the twenty-third release (code name ["Katherine Johnson"](https://en.wikipedia.org/wiki/Katherine_Johnson)) of the Einstein Toolkit, an open-source, community developed software infrastructure for relativistic astrophysics. The highlights of this release include:

* The inclusion of a new code in the Toolkit release, Kuibit
* The inclusion of a new code in the Toolkit release, RePrimAnd

In addition, bug fixes accumulated since the previous release in May 2021 have been included.

The Einstein Toolkit is a collection of software components and tools for simulating and analyzing general relativistic astrophysical systems that builds on numerous software efforts in the numerical relativity community including code to compute initial data parameters, the spacetime evolution codes Baikal, lean_public, and McLachlan, analysis codes to compute horizon characteristics and gravitational waves, the Carpet AMR infrastructure, and the relativistic magneto-hydrodynamics codes GRHydro and IllinoisGRMHD. The Einstein Toolkit also contains a 1D self-force code. For parts of the toolkit, the Cactus Framework is used as the underlying computational infrastructure providing large-scale parallelization, general computational components, and a model for collaborative, portable code development.

The Einstein Toolkit uses a distributed software model and its different modules are developed, distributed, and supported either by the core team of Einstein Toolkit Maintainers, or by individual groups. Where modules are provided by external groups, the Einstein Toolkit Maintainers provide quality control for modules for inclusion in the toolkit and help coordinate support. The Einstein Toolkit Maintainers currently involve staff and faculty from five different institutions, and host weekly meetings that are open for anyone to join.

Guiding principles for the design and implementation of the toolkit include: open, community-driven software development; well thought-out and stable interfaces; separation of physics software from computational science infrastructure; provision of complete working production code; training and education for a new generation of researchers.

For more information about using or contributing to the Einstein Toolkit, or to join the Einstein Toolkit Consortium, please visit our web pages at http://einsteintoolkit.org, or contact the users mailing list users@einsteintoolkit.org.

The Einstein Toolkit is primarily supported by NSF 2004157/2004044/2004311/2004879/2003893 (Enabling fundamental research in the era of multi-messenger astrophysics).

The Einstein Toolkit contains about 327 regression test cases. On a large portion of the tested machines, almost all of these tests pass, using both MPI and OpenMP parallelization.

## Contributors

This release includes contributions by
* Abhishek V. Joshi
* Erik Schnetter
* Gabriele Bozzola
* Leonardo Werneck
* Miguel Zilhao
* Roland Haas
* Steven R. Brandt
* Zachariah Etienne


## How to upgrade from Lorentz Release (ET_2021_05)

To upgrade from the previous release, use GetComponents with the new thornlist to check out the new version.

See the Download page (http://einsteintoolkit.org/download.html) on the Einstein Toolkit website for download instructions.

The SelfForce-1D code uses a single git repository, thus using 

  git pull; git checkout ET_2021_11

will update the code.

To install Kuibit, do the following:

  pip install --user -U kuibit==1.3.3

## Machine notes

### Supported (tested) machines include:

* Default Debian, Ubuntu, Fedora, CentOS 7, Mint, OpenSUSE and MacOS Catalina (MacPorts) installations
* Bluewaters
* Cori
* Expanse
* Frontera
* Queen Bee 2
* Queen Bee 3
* Stampede 2
* Sunrise
* SuperMUC-NG
* Summit
* Wheeler

### Note for individual machines:

* TACC machines: defs.local.ini needs to have `sourcebasedir = $WORK` and `basedir = $SCRATCH/simulations` configured for this machine. You need to determine $WORK and $SCRATCH by logging in to the machine.
* SuperMUC-NG: defs.local.ini needs to have `sourcebasedir = $HOME` and `basedir = $SCRATCH/simulations` configured for this machine. You need to determine $HOME and $SCRATCH by logging in to the machine.

All repositories participating in this release carry a branch ET_2021_05 marking this release. These release branches will be updated if severe errors are found.

## The "Katherine Johnson" Release Team on behalf of the Einstein Toolkit Consortium (2021-11-31)

* Steven R. Brandt,
* Roland Haas,
* Yosef Zlochower,
* Cheng-Hsin Cheng,
* Peter Diener,
* Alexandru Dima,
* William E. Gabella,
* Miguel Gracia-Linares,
* Gabriele Bozzola

Nov, 2021
