% vim: tw=0
# Release Announcement

We are pleased to announce the twenty-fifth release (code name "Sophie Kowalevski") of the Einstein Toolkit, an open-source, community developed software infrastructure for relativistic astrophysics. The major changes in this release include:

* many READS / WRITES statements were corrected
* there is now more robust testsuite error reporting
* kuibit has improved support for modern matplotlib versions
* a workaround for Intel 19+ compilers was added to GRHydro

Three new arrangements and thorns have been added:

* FLRW initial data solver for cosmological initial data
* NRPyEllipticET hyperbolic relaxation initial data solver for vacuum spacetimes
* The Canuda library of codes now supports complex and real scalar fields

New capabilities for existing codes:

* The SelFforce-1D code now supports gravitational perturbations using the Regge-Wheeler-Zerilli formalism

In addition, bug fixes accumulated since the previous release in May 2022 have been included.

The Einstein Toolkit is a collection of software components and tools for simulating and analyzing general relativistic astrophysical systems that builds on numerous software efforts in the numerical relativity community including code to compute initial data parameters, the spacetime evolution codes Baikal, lean_public, and McLachlan, analysis codes to compute horizon characteristics and gravitational waves, the Carpet AMR infrastructure, and the relativistic magneto-hydrodynamics codes GRHydro and IllinoisGRMHD. Data analysis and postprocessing is handled by the kuibit library. The Einstein Toolkit also contains a 1D self-force code. For parts of the toolkit, the Cactus Framework is used as the underlying computational infrastructure providing large-scale parallelization, general computational components, and a model for collaborative, portable code development.

The Einstein Toolkit uses a distributed software model and its different modules are developed, distributed, and supported either by the core team of Einstein Toolkit Maintainers, or by individual groups. Where modules are provided by external groups, the Einstein Toolkit Maintainers provide quality control for modules for inclusion in the toolkit and help coordinate support. The Einstein Toolkit Maintainers currently involve staff and faculty from five different institutions, and host weekly meetings that are open for anyone to join.

Guiding principles for the design and implementation of the toolkit include: open, community-driven software development; well thought-out and stable interfaces; separation of physics software from computational science infrastructure; provision of complete working production code; training and education for a new generation of researchers.

For more information about using or contributing to the Einstein Toolkit, or to join the Einstein Toolkit Consortium, please visit our web pages at http://einsteintoolkit.org, or contact the users mailing list users@einsteintoolkit.org.

The Einstein Toolkit is primarily supported by NSF 2004157/2004044/2004311/2004879/2003893 (Enabling fundamental research in the era of multi-messenger astrophysics).

The Einstein Toolkit contains about 336 regression test cases. On a large portion of the tested machines, almost all of these tests pass, using both MPI and OpenMP parallelization.

## Deprecated functionality
The following features are being marked as deprecated in this release and will be removed in the next release

* the "REQUIRES THORNS" statement in configuration.ccl has been deprecated and will be removed in the next release. Please use "REQUIRES" and "PROVIDES" instead.
* TmunuBase will no longer inherit from ADMBase and SaticConformal after this release, meaning thorns that inherit from TmunuBase will no longer automatically have access to the spacetime metric. Please explicitly inherit from ADMBase to access those variables.

## Contributors

Among the many contributors to the Einstein Toolkit and to this release in particular, important contributions to new components were made by the following authors:

* Alexandru Dima
* Anuj Kankani
* Cheng-Hsin Cheng
* Chloe Beth Richards
* Giuseppe Ficarra
* Hayley Macpherson
* Leonardo Werneck
* Liwei Ji
* Miguel Zilhao
* Samuel Cupp
* Taishi Ikeda

## How to upgrade from Riemann Release (ET_2022_05)

To upgrade from the previous release, use GetComponents with the new thornlist to check out the new version.

See the Download page (http://einsteintoolkit.org/download.html) on the Einstein Toolkit website for download instructions.

The SelfForce-1D code uses a single git repository, thus using 

  git pull; git checkout ET_2022_11

will update the code.

To install Kuibit, do the following:

  pip install --user -U kuibit==1.3.6

## Machine notes

### Supported (tested) machines include:

* Default Debian, Ubuntu, Fedora, Mint, and OpenSUSE installations
* Expanse
* Queen Bee 2
* Queen Bee 3
* Stampede 2
* Summit
* SuperMUC

### Note for individual machines:

* TACC machines: defs.local.ini needs to have `sourcebasedir = $WORK` and `basedir = $SCRATCH/simulations` configured for this machine. You need to determine $WORK and $SCRATCH by logging in to the machine.
* SuperMUC-NG: defs.local.ini needs to have `sourcebasedir = $HOME` and `basedir = $SCRATCH/simulations` configured for this machine. You need to determine $HOME and $SCRATCH by logging in to the machine.

All repositories participating in this release carry a branch ET_2022_11 marking this release. These release branches will be updated if severe errors are found.

## The "Sophie Kowalevski" Release Team on behalf of the Einstein Toolkit Consortium (2022-11-29)

* Roland Haas
* Allen Wen
* Anuj Kankani
* Bing-Jyun Tsao
* Cheng-Hsin Cheng
* Chi Tian
* Giuseppe Ficarra
* Hrishikesh Kalyanaraman
* Lisa Leung
* Nadine Kuo
* Peter Diener
* Steven R. Brandt
* Taishi Ikeda
* Zachariah Etienne

November 29, 2022
