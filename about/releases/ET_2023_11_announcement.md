# Release Announcement

We are pleased to announce the twenty-seventh release (code name "Lise Meitner") of the Einstein Toolkit, an open-source, community-developed software infrastructure for relativistic astrophysics. The major changes in this release include:

Several new arrangements and thorns have been added:

* CarpetX arrangement for the new AMReX-based mesh driver and supporting thorns
* GRHayLib for access to the General Relativistic Hydrodynamics Library (GRHayL)
* GRHayLID for simple GR(M)HD initial data using GRHayLib (e.g. Balsara tests, isotropic gas)
* GRHayLHD for GRHD evolution using GRHayL (equivalent to IllinoisGRMHD with no magnetic fields)
* GRHayLIDX for CarpetX version of GRHayLID; this thorn currently runs on the host, not the gpu
* GRHayLHDX for CarpetX version of GRHayLHD; this thorn currently runs on the host, not the gpu
* DNSdata for importing SGRID initial data

New capabilities for existing codes:

* Seed_Magnetic_Fields has been updated to support both TOV and BNS magnetic fields; explicit IllinoisGRMHD scheduling dependency removed to support broader usage; some parameter options are deprecated and will be removed in the next release

In addition, bug fixes accumulated since the previous release in May 2023 have been included.

The Einstein Toolkit is a collection of software components and tools for simulating and analyzing general relativistic astrophysical systems that builds on numerous software efforts in the numerical relativity community, including code to compute initial data parameters, the spacetime evolution codes Baikal, lean_public, and McLachlan, analysis codes to compute horizon characteristics and gravitational waves, the Carpet AMR infrastructure, and the relativistic (magneto)hydrodynamics codes GRHayLHD, GRHayLHDX, GRHydro, and IllinoisGRMHD. Data analysis and post-processing is handled by the kuibit library. The Einstein Toolkit also contains a 1D self-force code. For parts of the toolkit, the Cactus Framework is used as the underlying computational infrastructure, providing large-scale parallelization, general computational components, and a model for collaborative, portable code development.

The Einstein Toolkit uses a distributed software model, and its different modules are developed, distributed, and supported either by the core team of Einstein Toolkit Maintainers or by individual groups. Where modules are provided by external groups, the Einstein Toolkit Maintainers provide quality control for modules for inclusion in the toolkit and help coordinate support. The Einstein Toolkit Maintainers currently involve staff and faculty from five different institutions and host weekly meetings that are open for anyone to join.

Guiding principles for the design and implementation of the toolkit include: open, community-driven software development; well thought-out and stable interfaces; separation of physics software from computational science infrastructure; provision of complete working production code; training and education for a new generation of researchers.

For more information about using or contributing to the Einstein Toolkit, or to join the Einstein Toolkit Consortium, please visit our web pages at http://einsteintoolkit.org, or contact the users mailing list users@einsteintoolkit.org.

The Einstein Toolkit is primarily supported by NSF 2004157/2004044/2004311/2004879/2003893/2114582/2227105 (Enabling fundamental research in the era of multi-messenger astrophysics).

The Einstein Toolkit contains about 400 regression test cases. On a large portion of the tested machines, almost all of these tests pass, using both MPI and OpenMP parallelization.

## Deprecated functionality
* Seed_Magnetic_Fields to support the changes, parameter options were renamed; the old options are deprecated and will be removed in the next release
* Seed_Magnetic_Fields_BNS is slated to be removed in the next release, as its features have been merged into Seed_Magnetic_Fields

## Contributors

Among the many contributors to the Einstein Toolkit and to this release in particular, important contributions to new components were made by the following authors:

* Federico G. Lopez Armengol
* Steven R. Brandt
* Michail Chabanov
* Cheng-Hsin Cheng
* Samuel Cupp
* Alexandru Dima
* Jake Doherty
* Lorenzo Ennoggi
* Zachariah Etienne
* Roland Haas
* Terrence Pierre Jacques
* Liwei Ji
* Jay Kalinani
* Philipp Moesta
* Michal Pirog
* Lucas Timotheo Sanches
* Erik Schnetter
* Swapnil Shankar
* Wolfgang Tichy
* Leonardo Werneck

## How to upgrade from Schwarzschild Release (ET_2023_05)

To upgrade from the previous release, use GetComponents with the new thornlist to check out the new version.

See the Download page (http://einsteintoolkit.org/download.html) on the Einstein Toolkit website for download instructions.

The SelfForce-1D code uses a single git repository; thus, using

  git pull; git checkout ET_2023_11

will update the code.

To install Kuibit, do the following:

  pip install --user -U kuibit==1.4.0

## Machine notes

### Supported (tested) machines include:

* Debian, Ubuntu, Fedora, Mint, OpenSUSE, and macOS X installations with dependencies installed as prescribed in the official installation instructions
* Anvil
* Deep Bayou
* Delta
* Expanse
* Frontera
* Sunrise

### Note for individual machines:

* TACC machines: defs.local.ini needs to have `sourcebasedir = $WORK` and `basedir = $SCRATCH/simulations` configured for this machine. You need to determine $WORK and $SCRATCH by logging in to the machine.

All repositories participating in this release carry a branch ET_2023_11 marking this release. These release branches will be updated if severe errors are found.

## The "Lise Meitner" Release Team on behalf of the Einstein Toolkit Consortium (2023-12-14)

* Samuel Cupp
* Steven R. Brandt
* Peter Diener
* Zachariah Etienne
* Roland Haas
* Liwei Ji
* Deborah Ferguson
* Gabriele Bozzola
* Hyun Park

December 14, 2023
