# Release Announcement

We are pleased to announce the twenty-sixth release (code name "Karl Schwarzschild") of the Einstein Toolkit, an open-source, community-developed software infrastructure for relativistic astrophysics. The major changes in this release include:

* kuibit 1.4.0, which, in addition to bug fixes, improves support for working with horizon data, timer data, and higher rank objects (See [release notes](https://github.com/Sbozzolo/kuibit/releases/tag/1.4.0) for more details).

Two new arrangements and thorns have been added:

* The FUKA reader for numerical relativity initial data
* TwoPunctures_BBHSF for binary black hole with scalar fields initial data

New capabilities for existing codes:

* Baikal and BaikalVacuum have been updated to have READ/WRITE declarations for automated ghost zone synchronization. Several variable groups (such as RHS variables) are also no longer included in checkpoint/restart operations.
* particle_tracerET can now output the particles' four-velocity (contravariant and covariant); added support for binary output format; removed limit on the number of tracer particles

In addition, bug fixes accumulated since the previous release in November 2022 have been included.

The Einstein Toolkit is a collection of software components and tools for simulating and analyzing general relativistic astrophysical systems that builds on numerous software efforts in the numerical relativity community, including code to compute initial data parameters, the spacetime evolution codes Baikal, lean_public, and McLachlan, analysis codes to compute horizon characteristics and gravitational waves, the Carpet AMR infrastructure, and the relativistic magneto-hydrodynamics codes GRHydro and IllinoisGRMHD. Data analysis and post-processing is handled by the kuibit library. The Einstein Toolkit also contains a 1D self-force code. For parts of the toolkit, the Cactus Framework is used as the underlying computational infrastructure, providing large-scale parallelization, general computational components, and a model for collaborative, portable code development.

The Einstein Toolkit uses a distributed software model, and its different modules are developed, distributed, and supported either by the core team of Einstein Toolkit Maintainers or by individual groups. Where modules are provided by external groups, the Einstein Toolkit Maintainers provide quality control for modules for inclusion in the toolkit and help coordinate support. The Einstein Toolkit Maintainers currently involve staff and faculty from five different institutions and host weekly meetings that are open for anyone to join.

Guiding principles for the design and implementation of the toolkit include: open, community-driven software development; well thought-out and stable interfaces; separation of physics software from computational science infrastructure; provision of complete working production code; training and education for a new generation of researchers.

For more information about using or contributing to the Einstein Toolkit, or to join the Einstein Toolkit Consortium, please visit our web pages at http://einsteintoolkit.org, or contact the users mailing list users@einsteintoolkit.org.

The Einstein Toolkit is primarily supported by NSF 2004157/2004044/2004311/2004879/2003893 (Enabling fundamental research in the era of multi-messenger astrophysics).

The Einstein Toolkit contains about 350 regression test cases. On a large portion of the tested machines, almost all of these tests pass, using both MPI and OpenMP parallelization.

## Deprecated functionality
There are no features marked as deprecated in this release, to be removed in the next release.

## Contributors

Among the many contributors to the Einstein Toolkit and to this release in particular, important contributions to new components were made by the following authors:

* Cheng-Hsin Cheng
* Giuseppe Ficarra
* Gabriele Bozzola
* Leonardo Werneck
* Ludwig Jens Papenfort
* Samuel Cupp
* Samuel Tootle

## How to upgrade from Kowalevski Release (ET_2022_11)

To upgrade from the previous release, use GetComponents with the new thornlist to check out the new version.

See the Download page (http://einsteintoolkit.org/download.html) on the Einstein Toolkit website for download instructions.

The SelfForce-1D code uses a single git repository; thus, using

  git pull; git checkout ET_2023_05

will update the code.

To install Kuibit, do the following:

  pip install --user -U kuibit==1.4.0

## Machine notes

### Supported (tested) machines include:

* Debian, Ubuntu, Fedora, Mint, OpenSUSE, and macOS X installations with dependencies installed as prescribed in the official installation instructions
* Deep Bayou
* Delta
* Expanse
* Frontera
* Golub
* Queen Bee 2
* Queen Bee 3
* Stampede 2
* Summit
* Sunrise
* SuperMIC
* SuperMUC
* ThornyFlat

### Note for individual machines:

* TACC machines: defs.local.ini needs to have `sourcebasedir = $WORK` and `basedir = $SCRATCH/simulations` configured for this machine. You need to determine $WORK and $SCRATCH by logging in to the machine.
* SuperMUC-NG: defs.local.ini needs to have `sourcebasedir = $HOME` and `basedir = $SCRATCH/simulations` configured for this machine. You need to determine $HOME and $SCRATCH by logging in to the machine.

All repositories participating in this release carry a branch ET_2023_05 marking this release. These release branches will be updated if severe errors are found.

## The "Karl Schwarzschild" Release Team on behalf of the Einstein Toolkit Consortium (2023-05-24)

* Leonardo Werneck
* Samuel Cupp
* Beyhan Karakas
* Bing-Jyun Tsao
* Cheng-Hsin Cheng
* Jake Doherty
* Konrad Topolski
* Peter Diener
* Roland Haas
* Steven R. Brandt
* Terrence Pierre Jacques
* Thiago Assumpcao
* Giuseppe Ficarra
* Samuel Tootle
* Zachariah Etienne

May 24, 2023
