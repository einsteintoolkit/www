<?php header("Content-Type: text/html; charset=utf-8"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
  <title>Issue webhook received</title>
</head>
<body>
<pre>
<?php

include 'utils.php';

// from https://lornajane.net/posts/2017/handling-incoming-webhooks-in-php
$secret = $_GET['secret'];
// a random number to ensure that not everyone can use the hook
if ($secret != "ntdUMP30w0lR3KqXzZds7mftkZox8CHO23") {
  echo ("Invalid hook\n");
  http_response_code(403);
} elseif($json = json_decode(file_get_contents("php://input"), true)) {
  $data = $json;

  $subject = $data['subject'];
  // UTF8 characters in the subject must be encodded (confuse Mailman, not actually allowed)
  // this needs the php-imap extention. Alternatively one could use base64 like so:
  // "Subject: =?UTF-8?B?".base64_encode($subject)."?="
  // see https://stackoverflow.com/a/4389755
  if (! ctype_print($subject))
    $subject = "=?UTF-8?Q?".imap_8bit($subject)."?=";
  $text_msg = "HTML only email, please see https://einsteintoolkit.github.io/tests for output";
  $html_msg = $data['message'];

  $boundary = "----=_NextPart_" . md5(uniqid(rand()));
  $msg = "This is multipart message using MIME\n";
  $msg .= "--" . $boundary . "\n";
  $msg .= "Content-Type: text/plain; charset=UTF-8\n";
  $msg .= "Content-Transfer-Encoding: quoted-printable". "\n\n";
  $msg .= quoted_printable_encode($text_msg) . "\n";
  $msg .= "--" . $boundary . "\n";
  $msg .= "Content-Type: text/html; charset=UTF-8\n";
  $msg .= "Content-Transfer-Encoding: quoted-printable". "\n\n";
  $msg .= quoted_printable_encode($html_msg) . "\n";
  $msg .= "--" . $boundary . "--\n";

  $headers  = "From: jenkins@build-test.barrywardell.net\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: multipart/alternative; boundary=\"".$boundary."\"\r\n";
  $email = 'test@einsteintoolkit.org';
  $rc = mail($email, $subject, $msg, $headers);
  echo ("mail sent successfully:".$rc);
} else {
  echo ("Invalid request\n");
  http_response_code(400);
}
?>
</pre>

</body>
</html>

